Justin Evan Belga: 

Good day!

It said do not invest more than two hours on this so I just made some basic changes and I'll just put my comments and suggestions on the codes here.

I made some changes on the Formatting of the Codes. Added some lines and spaces.
Then the next thing I noticed was the return types and return format, so I added a HttpCodeHelper Class to facilitate a uniform format of return data to the API
consumer. 
I did not refactor the codes in them because one and a half hours was not enough for me to understand the codes completely and design new DRY functions 
to address the issues i saw.

I understand that the app kinda enables a customer to hire people to help them with their needs, with pay or for free, like to hire/book a translator, probably for foreigners
visiting a country. But basically if this was a real scenario, I'll ask a lot of things first so that I can make those if,else and other parts there into more optimized
and reusable functions.

So my suggestion/comment is the functions are very long. Second the Class itself is so long.
Some of the things that can be done are to: remove the Authorization checks from the Repository and put it into MiddleWares so that even before the request gets to the
controller we can already determine if the User is authorized to make a booking, change/add/delete a data, and so on; then we can use Laravel Validation to do the basic
request payload validations, like the simple task of checking if a certain object/index exists in the Payload; lastly is to study the BookingRepository based on the actual
requirements and design better and more reusable Functions that can even be extracted into a completely different class and used by the other Repository Classes.


================================================================================
Instructions

Choose ONE of the following tasks.
Please do not invest more than two hours on this.
Upload your results to a Github Gist, for easier sharing and reviewing.

Thank you and good luck!



Code to refactor
=================
1) app/Http/Controllers/BookingController.php
2) app/Repository/BookingRepository.php

Code to write tests
=====================
3) App/Helpers/TeHelper.php method willExpireAt
4) App/Repository/UserRepository.php, method createOrUpdate
