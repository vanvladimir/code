<?php
/**
 * Created by PhpStorm.
 * User: ALPHA
 * Date: 6/28/2017
 * Time: 5:10 PM
 */

namespace DTApi\Http\Controllers;


class HttpCodeHelper
{
    public static function badRequest($message = null)
    {
        if(!$message){
            $message = "Bad Request: The request cannot be fulfilled due to bad syntax.";
        }

        return response(array(
            "code" => 400,
            "message" => $message
        ));
    }

    public static function conflict($message = null)
    {
        if(!$message){
            $message = "Conflict: there has been a conflict in the request.";
        }

        return response(array(
            "code" => 409,
            "message" => $message
        ));
    }

    public static function success($data = null, $message = null)
    {
        if(!$message){
            $message = "Success: Your request has been processed successfully.";
        }

        return response(array(
            "code" => 200,
            "message" => $message,
            "data" => $data
        ));
    }

    public static function notFound($message = null)
    {
        if(!$message){
            $message = "The resource you requested was not found.";
        }

        return response(array(
            "code" => 404,
            "message" => $message
        ));
    }

    public static function created($id, $message = null)
    {
        if(!$message){
            $message = "Success: the resource has been created.";
        }

        return response(array(
            "code" => 201,
            "message" => $message,
            "data" => $id
        ));
    }

    public static function updated($message = null)
    {
        if(!$message){
            $message = "Success: the resource has been updated.";
        }

        return response(array(
            "code" => 200,
            "message" => $message
        ));
    }

    public static function unexpectedError()
    {
        return response(array(
            "code" => 500,
            "message" => "Error: an unexpected error was encountered. Contact the administrator for help."
        ));
    }
}