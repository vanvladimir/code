<?php

namespace DTApi\Http\Controllers;

use DTApi\Models\Job;
use DTApi\Http\Requests;
use DTApi\Models\Distance;
use Illuminate\Http\Request;
use DTApi\Repository\BookingRepository;

/**
 * Class BookingController
 * @package DTApi\Http\Controllers
 */
class BookingController extends Controller
{

    /**
     * @var BookingRepository
     */
    protected $repository;

    /**
     * BookingController constructor.
     * @param BookingRepository $bookingRepository
     */
    public function __construct(BookingRepository $bookingRepository)
    {
        $this->repository = $bookingRepository;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($user_id = $request->get('user_id')) {

            $response = $this->repository->getUsersJobs($user_id);

        } elseif ($request->__authenticatedUser->user_type == env('ADMIN_ROLE_ID') || $request->__authenticatedUser->user_type == env('SUPERADMIN_ROLE_ID')) {

            $response = $this->repository->getAll($request);

        } else {

            return HttpCodeHelper::badRequest();

        }

        return HttpCodeHelper::success($response);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $job = $this->repository->with('translatorJobRel.user')->find($id);

        if ($job) {
            return HttpCodeHelper::success($job);
        } else {
            return HttpCodeHelper::notFound();
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $response = $this->repository->store($request->__authenticatedUser, $data);

        if ($response['status'] == 'success') {
            return HttpCodeHelper::created($response);
        } else{
            return HttpCodeHelper::badRequest($response);
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function update($id, Request $request)
    {
        $data = $request->all();

        $cuser = $request->__authenticatedUser;

        $response = $this->repository->updateJob($id, array_except($data, ['_token', 'submit']), $cuser);

        if($response['status'] == 'Updated'){
            return HttpCodeHelper::updated();
        }elseif($response['status'] == 'jobNotFound'){
            return HttpCodeHelper::notFound("The Job with id = " . $id . "was not found.");
        }else {
            return HttpCodeHelper::unexpectedError();
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function immediateJobEmail(Request $request)
    {
        $adminSenderEmail = config('app.adminemail');
        $data = $request->all();

        $response = $this->repository->storeJobEmail($data);

        if($response['status'] == 'success'){
            return HttpCodeHelper::success($response);
        } else {
            return HttpCodeHelper::unexpectedError();
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getHistory(Request $request)
    {
        if ($user_id = $request->get('user_id')) {

            // I transferred the codes that fetches the page here.
            $page = $request->get('page');

            $response = $this->repository->getUsersJobsHistory($user_id, $page); // It is not advisable to pass the $request object to the repository.

            return HttpCodeHelper::success($response);
        }

        return HttpCodeHelper::success(null);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function acceptJob(Request $request)
    {
        $data = $request->all();
        $user = $request->__authenticatedUser;

        $response = $this->repository->acceptJob($data, $user);

        if($response['status'] == 'success'){
            return HttpCodeHelper::success($response);
        }elseif ($response['status'] == 'fail'){
            return HttpCodeHelper::conflict($response);
        }else{
            return HttpCodeHelper::unexpectedError();
        }

    }

    public function acceptJobWithId(Request $request)
    {
        $data = $request->get('job_id');
        $user = $request->__authenticatedUser;

        $response = $this->repository->acceptJobWithId($data, $user);

        if($response['status'] == 'success'){
            return HttpCodeHelper::success($response);
        }elseif ($response['status'] == 'fail'){
            return HttpCodeHelper::conflict($response);
        }else{
            return HttpCodeHelper::unexpectedError();
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function cancelJob(Request $request)
    {
        $data = $request->all();
        $user = $request->__authenticatedUser;

        $response = $this->repository->cancelJobAjax($data, $user);

        if($response['status'] == 'success'){
            return HttpCodeHelper::success($response);
        }elseif ($response['status'] == 'fail'){
            return HttpCodeHelper::badRequest($response);
        }else{
            return HttpCodeHelper::unexpectedError();
        }
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function endJob(Request $request)
    {
        $data = $request->all();

        $response = $this->repository->endJob($data);

        if($response['status'] == 'success'){
            return HttpCodeHelper::success($response);
        }else{
            return HttpCodeHelper::unexpectedError();
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function customerNotCall(Request $request)
    {
        $data = $request->all();

        $response = $this->repository->customerNotCall($data);

        if($response['status'] == 'success'){
            return HttpCodeHelper::success($response);
        }else{
            return HttpCodeHelper::unexpectedError();
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getPotentialJobs(Request $request)
    {
        $data = $request->all();
        $user = $request->__authenticatedUser;

        $response = $this->repository->getPotentialJobs($user);

        return HttpCodeHelper::success($response);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function distanceFeed(Request $request)
    {
        $data = $request->all();

        if (isset($data['distance']) && $data['distance'] != "") {
            $distance = $data['distance'];
        } else {
            $distance = "";
        }

        if (isset($data['time']) && $data['time'] != "") {
            $time = $data['time'];
        } else {
            $time = "";
        }

        if (isset($data['jobid']) && $data['jobid'] != "") {
            $jobid = $data['jobid'];
        }

        if (isset($data['session_time']) && $data['session_time'] != "") {
            $session = $data['session_time'];
        } else {
            $session = "";
        }

        if ($data['flagged'] == 'true') {
            if ($data['admincomment'] == '') {
                return "Please, add comment";
            }
            $flagged = 'yes';
        } else {
            $flagged = 'no';
        }

        if ($data['manually_handled'] == 'true') {
            $manually_handled = 'yes';
        } else {
            $manually_handled = 'no';
        }

        if ($data['by_admin'] == 'true') {
            $by_admin = 'yes';
        } else {
            $by_admin = 'no';
        }

        if (isset($data['admincomment']) && $data['admincomment'] != "") {
            $admincomment = $data['admincomment'];
        } else {
            $admincomment = "";
        }

        if ($time || $distance) {
            $affectedRows = Distance::where('job_id', '=', $jobid)->update(array(
                'distance' => $distance,
                'time' => $time
            ));
        }

        if ($admincomment || $session || $flagged || $manually_handled || $by_admin) {

            $affectedRows1 = Job::where('id', '=', $jobid)->update(array(
                'admin_comments' => $admincomment,
                'flagged' => $flagged,
                'session_time' => $session,
                'manually_handled' => $manually_handled,
                'by_admin' => $by_admin
            ));

        }

        return response('Record updated!');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function reopen(Request $request)
    {
        $data = $request->all();
        $response = $this->repository->reopen($data);

        return HttpCodeHelper::success($response);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function resendNotifications(Request $request)
    {
        $data = $request->all();
        $job = $this->repository->find($data['jobid']);
        $job_data = $this->repository->jobToData($job);
        $this->repository->sendNotificationTranslator($job, $job_data, '*');

        return HttpCodeHelper::success(['success' => 'Push sent']);
    }

    /**
     * Sends SMS to Translator
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function resendSMSNotifications(Request $request)
    {
        $data = $request->all();
        $job = $this->repository->find($data['jobid']);
        $job_data = $this->repository->jobToData($job);

        try {
            $this->repository->sendSMSNotificationToTranslator($job);
            return HttpCodeHelper::success(['success' => 'SMS sent']);
        } catch (\Exception $e) {
            return HttpCodeHelper::success(['success' => $e->getMessage()]);
        }
    }

}
